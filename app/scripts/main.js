jQuery(function ($) {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  new Swiper('.t-what__cards', {
    navigation: {
      nextEl: '.t-what .swiper-button-next',
      prevEl: '.t-what .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 100,
  });

  new Swiper('.t-reviews-this__cards', {
    pagination: {
      el: '.t-reviews .swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.t-reviews .swiper-button-next',
      prevEl: '.t-reviews .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 100,
  });

  new Swiper('.t-news__cards', {
    pagination: {
      el: '.t-news .swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.t-news .swiper-button-next',
      prevEl: '.t-news .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 70,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 2,
      },
    },
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.t-modal').toggle();
  });

  $('.t-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 't-modal__centered') {
      $('.t-modal').hide();
    }
  });

  $('.t-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.t-modal').hide();
  });

  $('.open-popup').on('click', function (e) {
    e.preventDefault();

    $('.t-popup').toggle();
  });

  $('.t-popup__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 't-popup__centered') {
      $('.t-popup').hide();
    }
  });

  $('.t-popup__close').on('click', function (e) {
    e.preventDefault();
    $('.t-popup').hide();
  });

  // handle links with @href started with '#' only
  $(document).on('click', 'a[href^="#"]', function (e) {
    // target element id
    var id = $(this).attr('href');

    // target element
    var $id = $(id);
    if ($id.length === 0) {
      return;
    }

    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();

    // top position relative to the document
    var pos = $id.offset().top;

    // animated top scrolling
    $('body, html').animate({ scrollTop: pos });
  });

  $(window).on('load', function () {
    if ($(window).width() < 1200) {
      $('.t-reviews-this__row').detach().appendTo('.t-reviews-this__cards');
    }
  });

  new WOW().init();
});
